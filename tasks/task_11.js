// Каррирование. Написать ф-ию, которая может вызываться неограниченное количество раз и считать общую сумму всех переданных параметров:
// add(4)(3)(1) => 8
// add(4)(3)(1)(2)(3) => 13
// add(1)(1)(1)(1)(1)(1)(1)(1)(1)(1) => 10

// function add(num) {
//   function f() {}
//   num = parseInt(num) || 0;
//   return (arg) => add(num + arg);
// }

const add = (a = 0) => {
  let sum = a;
  const func = (b = 0) => {
    sum += b;
    return func;
  };

  func.toString = () => sum;
  func.valueOf = () => sum;

  return func;
};

export { add };
