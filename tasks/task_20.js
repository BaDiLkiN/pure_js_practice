// Написать функцию persistence, которая принимает положительный параметр num и возвращает число, которое нужно умножить цифры в num, пока не дойдете до единственной цифры.

// Примеры:
//  persistence(39) === 3     // because 3*9 = 27, 2*7 = 14, 1*4=4
// and 4 has only one digit

//  persistence(999) === 4     // because 9*9*9 = 729, 7*2*9 = 126,
// 1*2*6 = 12, and finally 1*2 = 2

//  persistence(4) === 0         // because 4 is already a one-digit number

function persistence(num, count = 0) {
  if (num < 10) {
    return count;
  }
  const n = num.toString().split('');
  const res = n.reduce((prev, curr) => prev * curr);
  if (res > 9) {
    return persistence(res, ++count);
  }
  return ++count;
}

export { persistence };
