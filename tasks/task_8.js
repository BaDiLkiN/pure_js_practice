// Функция принимает массив meetups,
// и возвращает суммарное количество человек, находящихся на активных митапах

// membersOnActiveMeetups(meetups); // 1500

// Пример:
// const meetups = [
//   { name: 'JavaScript', isActive: true, members: 100 },
//   { name: 'Angular', isActive: true, members: 900 },
//   { name: 'Node', isActive: false, members: 600 },
//   { name: 'React', isActive: true, members: 500 },
// ];
// membersOnActiveMeetups(meetups); // 1500

function membersOnActiveMeetups(arr) {
  return arr
    .filter((meet) => meet.isActive == true)
    .reduce((acc, { members }) => {
      return acc + members;
    }, 0);
}

export { membersOnActiveMeetups };
