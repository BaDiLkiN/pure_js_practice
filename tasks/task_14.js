// Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.

// Например:

// let arr = [5, 3, 8, 1];

// let filtered = filterRange(arr, 1, 4);

// alert( filtered ); // 3,1

function filterRange(arr, a, b) {
  const min = a > b ? b : a;
  const max = a < b ? b : a;
  return arr.filter((num) => num >= min && num <= max);
}

export { filterRange };
