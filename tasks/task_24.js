// Дана строка. Вернуть длину самого короткого слова
// Пример:
// getLengthOfShortestWord(‘Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.’)     // => 2

function getLengthOfShortestWord(str) {
  let res;
  let min = str.length;
  res = str.replace(/\,|\./gm, '');
  res.split(' ').forEach((word) => {
    if (word.length < min) {
      return (min = word.length);
    } else {
      return min;
    }
  });
  return min;
}

export { getLengthOfShortestWord };
