// Реализовать функцию indexOfAll.
// Первый аргумент - массив, второй - значение

// Функция возвращает массив со всеми индексами, которые соответствуют переданному значению

// console.log(indexOfAll([1, 2, 3, 1, 2, 3], 1)); -> [0, 3]
// console.log(indexOfAll([1, 2, 3], 4)); -> []

function indexOfAll(arr, val) {
  return arr.reduce((acc, curr, i) => {
    return curr === val ? [...acc, i] : acc;
  }, []);
}

export { indexOfAll };
