// Функция принимает 2 массива.
// Возвращает новый массив, который состоит только из тех элементов,
// которые встретились в одном массиве, но не встретились в другом

// console.log(arrayDiff([1, 2, 3], [1, 2, 4])); -> [3, 4]
// console.log(arrayDiff([1, 3, 3, 4], [1, 3, '4'])); -> [4, '4']

function arrayDiff(firstArr, secondArr) {
  const firstSet = new Set(firstArr);
  const secondSet = new Set(secondArr);
  secondSet.forEach((v) => {
    if (firstSet.has(v)) {
      firstSet.delete(v);
    } else {
      firstSet.add(v);
    }
  });
  return [...firstSet];
}

export { arrayDiff };
