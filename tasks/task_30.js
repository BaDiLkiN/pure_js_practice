// Написать функцию, которая принимает строковый параметр и переворачивает каждое слово в строке. Все пробелы в строке следует сохранить.

// Examples
// "This is an example!" ==> "sihT si na !elpmaxe"
// "double  spaces"      ==> "elbuod  secaps"

function esrever(str) {
  const res = str
    .split(' ')
    .map((word) => [...word].reverse().join(''))
    .join(' ');
  return res;
}

export { esrever };
