// Реализовать функцию uniqueInOrder, которая принимает в качестве аргумента последовательность и возвращает список элементов
// без каких-либо элементов с одинаковым значением рядом друг с другом и с сохранением исходного порядка элементов.

// Примеры:
// uniqueInOrder('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
// uniqueInOrder('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
// uniqueInOrder([1,2,2,3,3])       == [1,2,3]

function uniqueInOrder(something) {
  let intoArr;
  if (typeof something === 'string') {
    intoArr = something.split('');
  } else {
    intoArr = something;
  }
  intoArr.map((val, i) => {
    if (val == intoArr[i + 1]) {
      intoArr[i] = '';
    }
  });
  const filtered = intoArr.filter(Boolean);
  return filtered;
}

export { uniqueInOrder };
