// Создать функцию, которая принимает римское число в качестве аргумента и возвращает его значение как числовое десятичное целое число. Не нужно проверять форму римской цифры.

// Римские цифры записываются путем выражения каждой десятичной цифры числа, которые нужно кодировать отдельно, начиная с крайней левой цифры. Таким образом, 1990 год отображается как «MCMXC» (1000 = M, 900 = CM, 90 = XC), а 2008 год отображается как «MMVIII» (2000 = MM, 8 = VIII). В римской числе 1666 года «MDCLXVI» каждая буква используется в порядке убывания.

// Пример:

// solution('XXI');     // 21

// Помощь:
// Symbol    Value
// I                  1
// V              5
// X              10
// L              50
// C              100
// D        500
// M              1,000

// let res = 0;
// for (let i = 0; i < romanENKO.length; i++) {
//   const curr = +data[romanENKO[i]];
//   const next = +data[romanENKO[i + 1]];
//   if (curr < next) {
//     res -= next;
//   } else {
//     res += curr;
//   }
// }
// return res;

function solution(romanENKO) {
  const data = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000 };
  let res = 0;
  let curr,
    last = 0;
  romanENKO
    .split('')
    .reverse()
    .forEach((e) => {
      curr = data[e];
      if (curr >= last) {
        res += curr;
      } else {
        res -= curr;
      }
      last = curr;
    });

  return res;
}

export { solution };
