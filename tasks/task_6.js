// Реализовать функцию without.
// Первый аргумент - массив, второй и последующие - значения

// Функция возвращает новый массив, который наполнен теми значениями,
// которые не передавались как второй и последующие аргументы функции

// console.log(without([2, 1, 2, 3], 1, 2)) -> [3]
// console.log(without([2, 1, 10, 20, 5], 1, 2, 5)) -> [10, 20]

function without(arr, ...values) {
  const setArr = new Set(arr);
  values.forEach((v) => {
    if (setArr.has(v)) {
      setArr.delete(v);
    } else {
      setArr.add(v);
    }
  });
  return [...setArr];
}

export { without };
