import { any } from './tasks/task_1.js';
import { arrayDiff } from './tasks/task_2.js';
import { forEachRight } from './tasks/task_3.js';
import { union } from './tasks/task_4.js';
import { createGenerator } from './tasks/task_5.js';
import { without } from './tasks/task_6.js';
import { indexOfAll } from './tasks/task_7.js';
import { membersOnActiveMeetups } from './tasks/task_8.js';
import { factory } from './tasks/task_9.js';
import { add } from './tasks/task_11.js';
import { filterRange } from './tasks/task_14.js';
import { unique } from './tasks/task_15.js';
import { sumTo } from './tasks/task_16.js';
import { spinWords } from './tasks/task_17.js';
import { filter_list } from './tasks/task_18.js';
import { multi } from './tasks/task_19.js';
import { objct } from './tasks/task_10.js';
import { getMaxSubSum } from './tasks/task_12.js';
import { camelize } from './tasks/task_13.js';
import { persistence } from './tasks/task_20.js';
import { toInt } from './tasks/task_21.js';
import { divisors } from './tasks/task_22.js';
import { getLengthOfShortestWord } from './tasks/task_24.js';
import { isTriangle } from './tasks/task_25.js';
import { getMiddle } from './tasks/task_26.js';
import { minMax } from './tasks/task_29.js';
import { sortArr } from './tasks/task_27.js';
import { similarCount } from './tasks/task_28.js';
import { esrever } from './tasks/task_30.js';
import { uniqueInOrder } from './tasks/task_31.js';
import { solution } from './tasks/task_32.js';

// Task 1
console.group('Task 1');
console.log(any([0, 1, 2, 0], (x) => x >= 2)); // -> true
console.log(any([0, 0, 1, 0])); // -> true
console.log(any([0, 0, 0, 0])); // -> false
console.groupEnd();

// Task 2
console.group('Task 2');
console.log(arrayDiff([1, 2, 3], [1, 2, 4])); // -> [3, 4]
console.log(arrayDiff([1, 3, 3, 4], [1, 3, '4'])); // -> [4, '4']
console.groupEnd();

// Task 3
console.group('Task 3');
forEachRight([1, 2, 3, 4], (val) => console.log(val)); // В консоль 4 3 2 1
console.groupEnd();

// Task 4
console.group('Task 4');
console.log(union([5, 1, 2, 3, 3], [4, 3, 2])); // -> [5, 1, 2, 3, 4];
console.log(union([5, 1, 3, 3, 4], [1, 3, 4])); // -> [5, 1, 3, 4]
console.groupEnd();

// Task 5 (NOT DONE YET)
console.group('Task 5');
console.error('not done');
const generator = createGenerator([1, '6', 3, 2]);
generator.next(); // -> 1
generator.next(); // -> '6'
generator.next(); // -> 3
generator.next(); // -> 2
generator.next(); // -> 'Complete!'
generator.next(); // -> 'Complete!'
console.groupEnd();

// Task 6
console.group('Task 6');
console.log(without([2, 1, 2, 3], 1, 2)); // -> [3]
console.log(without([2, 1, 10, 20, 5], 1, 2, 5)); // -> [10, 20]
console.groupEnd();

//Task 7
console.group('Task 7');
console.log(indexOfAll([1, 2, 3, 1, 2, 3], 1)); // -> [0, 3]
console.log(indexOfAll([1, 2, 3], 4)); // -> []
console.groupEnd();

//Task 8
console.group('Task 8');
const meetups = [
  { name: 'JavaScript', isActive: true, members: 100 },
  { name: 'Angular', isActive: true, members: 900 },
  { name: 'Node', isActive: false, members: 600 },
  { name: 'React', isActive: true, members: 500 },
];
console.log(membersOnActiveMeetups(meetups));
console.groupEnd();

//Task 9
console.group('Task 9');
const obj = factory(12, 23, 'myFunc');
console.log(obj.x, obj.y, obj.myFunc());
console.groupEnd();

//Task 10
console.group('Task 10');
console.log(`Name: ${objct}`); // Name: Obj-name
console.log(+objct); // 0
console.log(objct + 10); // 10
console.groupEnd();

//Task 11
console.group('Task 11');
console.log(+add(4)(3)(1)); //=> 8
console.log(+add(4)(3)(1)(2)(3)); //=> 13
console.log(+add(1)(1)(1)(1)(1)(1)(1)(1)(1)(1)); // => 10
console.groupEnd();

//Task 12
console.group('Task 12');
console.log(getMaxSubSum([-1, 2, 3, -9])); // => 5 (сумма выделенных)
console.log(getMaxSubSum([2, -1, 2, 3, -9])); // => 6
console.log(getMaxSubSum([-1, 2, 3, -9, 11])); // => 11
console.log(getMaxSubSum([-2, -1, 1, 2])); // => 3
console.log(getMaxSubSum([100, -9, 2, -3, 5])); // => 100
console.log(getMaxSubSum([1, 2, 3])); // => 6 (берём все)
console.groupEnd();

//Task 13
console.group('Task 13');
console.log(camelize('background-color'));
console.log(camelize('list-style-image'));
console.log(camelize('-webkit-transition'));
console.groupEnd();

//Task 14
console.group('Task 14');
const arr = [5, 3, 8, 1];
const filtered = filterRange(arr, 1, 4);
console.log(filtered); // => 3,1
console.groupEnd();

//Task 15
console.group('Task 15');
const strings = ['aaa', 'aaa', 'zzz', 'xxx', 'aaa', 'bbb', 'aaa', 'xxx', 'ccc'];
console.log(unique(strings)); // => [‘aaa’, ‘zzz’, ‘xxx’, ‘bbb’, ‘ccc’]
console.groupEnd();

//Task 16
console.group('Task 16');
console.log(sumTo(3)); // => 6
console.log(sumTo(4)); // => 10
console.groupEnd();

//Task 17
console.group('Task 17');
console.log(spinWords('Hey fellow warriors', 5)); // => returns "Hey wollef sroirraw"
console.log(spinWords('This is a test', 10)); // => returns "This is a test"
console.log(spinWords('This is another test', 3)); // => returns "sihT is rehtona tset"
console.groupEnd();

//Task 18
console.group('Task 18');
console.log(filter_list([1, 2, 'a', 'b'])); // => [1, 2];
console.log(filter_list([1, 'a', 'b', 0, 15])); // => [1, 0, 15];
console.log(filter_list([1, 2, 'aasf', '1', '123', 123])); // => [1, 2, 123];
console.groupEnd();

//Task 19
console.group('Task 19');
console.log(multi(9119)); // => 811181
console.groupEnd();

//Task 20
console.group('Task 20');
console.log(persistence(39)); // === 3     // because 3*9 = 27, 2*7 = 14, 1*4=4
// and 4 has only one digit

console.log(persistence(999)); // === 4     // because 9*9*9 = 729, 7*2*9 = 126,
// 1*2*6 = 12, and finally 1*2 = 2

console.log(persistence(4)); // === 0         // because 4 is already a one-digit number
console.groupEnd();

//Task 21
console.group('Task 21');
console.log(toInt([0, 0, 0, 1])); // => 1
console.log(toInt([0, 0, 1, 0])); // => 2
console.log(toInt([0, 1, 0, 1])); // => 5
console.log(toInt([1, 0, 0, 1])); // => 9
console.log(toInt([0, 0, 1, 0])); // => 2
console.log(toInt([0, 1, 1, 0])); // => 6
console.log(toInt([1, 1, 1, 1])); // => 15
console.log(toInt([1, 0, 1, 1])); // => 11
console.groupEnd();

//Task 22
console.group('Task 22');
console.log(divisors(12)); // => should return [2,3,4,6]
console.log(divisors(25)); // => should return [5]
console.log(divisors(13)); // => should return "13 is prime"
console.groupEnd();

//Task 23
console.group('Task 23');
console.log('Skip');
console.groupEnd();

//Task 24
console.group('Task 24');
console.log(
  getLengthOfShortestWord(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  )
); // => 2
console.groupEnd();

//Task 25
console.group('Task 25');
console.log(isTriangle(1, 2, 2)); // => true
console.log(isTriangle(7, 2, 2)); // => false
console.groupEnd();

//Task 26
console.group('Task 26');
console.log(getMiddle('test')); // => should return "es"
console.log(getMiddle('testing')); // => should return "t"
console.log(getMiddle('middle')); // => should return "dd"
console.log(getMiddle('A')); // => should return "A"
console.groupEnd();

//Task 27
console.group('Task 27');
console.log(sortArr([7, 1])); //  =>  [1, 7]
console.log(sortArr([5, 8, 6, 3, 4])); //  =>  [3, 8, 6, 5, 4]
console.log(sortArr([9, 8, 7, 6, 5, 4, 3, 2, 1, 0])); //  =>  [1, 8, 3, 6, 5, 4, 7, 2, 9, 0]
console.groupEnd();

//Task 28
console.group('Task 28');
console.log(similarCount('abcde')); //-> 0 # no characters repeats more than once
console.log(similarCount('aabbcde')); //-> 2 # 'a' and 'b'
console.log(similarCount('aabBcde')); //-> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
console.log(similarCount('indivisibility')); //-> 1 # 'i' occurs six times
console.log(similarCount('Indivisibilities')); //-> 2 # 'i' occurs seven times and 's' occurs twice
console.log(similarCount('aA11')); //-> 2 # 'a' and '1'
console.log(similarCount('ABBA')); //-> 2 # 'A' and 'B' each occur twice
console.groupEnd();

//Task 29
console.group('Task 29');
console.log(minMax([1, 2, 3, 4, 5])); // => [1,5]
console.log(minMax([2334454, 5])); // => [5, 2334454]
console.log(minMax([1])); // => [1, 1]
console.groupEnd();

//Task 30
console.group('Task 30');
console.log(esrever('This is an example!'));
console.log(esrever('double  spaces'));
console.groupEnd();

//Task 31
console.group('Task 31');
console.log(uniqueInOrder('AAAABBBCCDAABBB'));
console.log(uniqueInOrder('ABBCcAD'));
console.log(uniqueInOrder([1, 2, 2, 3, 3]));
console.groupEnd();

//Task 32
console.group('Task 32');
console.log(solution('XXI')); //21
console.log(solution('XX')); //20
console.log(solution('MXXI')); // 1021
console.log(solution('MCMXC')); //1990;
console.log(solution('MMVIII')); //2008;
console.groupEnd();
